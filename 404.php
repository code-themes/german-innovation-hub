<?php get_header(); ?>
	
	<div id="content">

		<div id="inner-content" class="wrap">

			<main id="main" class="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="https://schema.org/Blog">

				<article id="post-not-found" class="hentry">

					<header class="heading">
			            <div class="heading__container">

			                <div class="heading__heading-wrapper">
			                    <?php get_template_part( 'templates/header', 'title'); ?>
			                </div>
			                <?php /*<div class="heading__subheading-wrapper">
			                    <h2 class="heading__subheading"></h2>
			                </div>*/ ?>

			            </div>
			        </header>

			        <div class="content">
			            <div class="content__container">

			                <div class="row justify-content-center">
			                    <div class="col-md-8 clear-content-margins" itemprop="articleBody">

			                    	<p><?php _e( 'I\'m sorry Dave, I\'m afraid I can\'t do that.', 'platetheme' ); ?></p>
			                        <p><?php _e( 'We couldn\'t find what you are looking for, please try again.', 'platetheme' ); ?></p>

			                    </div>
			                </div>

			            </div>
			        </div>

				</article>

			</main>

		</div>

	</div>

    <?php get_sidebar(); ?>

<?php get_footer(); ?>
