<?php
/**
 * 
 * Template Part: The Loop
 * Description: Loop code for Pages and Posts.
 * 
 * @example <?php get_template_part( 'templates/loop'); ?>
 * 
 * @author  Joshua Michaels for studio.bio <info@studio.bio>
 * @since   1.0.0
 * @version 1.3
 * @license WTFPL
 * 
 * @see     https://konstantin.blog/2013/get_template_part/
 *          http://buildwpyourself.com/get-template-part/
 * 
 */
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="https://schema.org/BlogPosting">

        <header class="heading">
            <div class="heading__container">

                <div class="heading__heading-wrapper">
                    <h1 class="heading__heading" itemprop="headline"><?php the_title(); ?></h1>
                </div>
                <?php /*<div class="heading__subheading-wrapper">
                    <h2 class="heading__subheading"></h2>
                </div>*/ ?>

            </div>
        </header>

        <div class="content">
            <div class="content__container">

                <div class="row justify-content-center">
                    <div class="col-md-8 clear-content-margins" itemprop="articleBody">

                        <?php the_content(); ?>

                    </div>
                </div>

            </div>
        </div>

        <?php if( post_type_supports( get_post_type(), 'comments' ) ) {

            if( comments_open() ) {

                comments_template();
                
            }

        } ?>

    </article>

<?php endwhile; ?>

    <?php plate_page_navi( $wp_query ); ?>

<?php else : ?>

    <?php get_template_part( 'templates/404'); ?>

<?php endif; ?>