<?php
/**
 * 
 * Template Part: Single
 * Description: Loop code for Single Posts.
 * 
 * @example <?php get_template_part( 'templates/single', 'loop'); ?>
 * 
 * @author  Joshua Michaels for studio.bio <info@studio.bio>
 * @since   1.0.0
 * @version 1.3
 * @license WTFPL
 * 
 * @see     https://konstantin.blog/2013/get_template_part/
 *          http://buildwpyourself.com/get-template-part/
 * 
 */
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemprop="blogPost" itemtype="https://schema.org/BlogPosting">

        <header class="heading">
            <div class="heading__container">

                <div class="heading__heading-wrapper">
                    <h1 class="heading__multi-line-heading" itemprop="headline" rel="bookmark"><span><?php the_title(); ?></span></h1>
                </div>
                <?php /*<div class="heading__subheading-wrapper">
                    <h2 class="heading__subheading"><?php get_template_part( 'templates/byline'); ?></h2>
                </div>*/ ?>

            </div>
        </header>

        <div class="content" itemprop="articleBody">
            <div class="container content__container content__container--blog">

                <?php if (has_post_format()) { 
                    get_template_part( 'format', get_post_format() ); 
                } ?>
            
                <?php the_content(); ?>

            </div>
        </div>

    </article>

<?php endwhile; ?>

<?php else : ?>

    <?php get_template_part( 'templates/404'); ?>

<?php endif; ?>