<div class="insight">
    <?php if (has_post_thumbnail()): ?>
        <div class="insight__media">
            <div class="insight__image-wrapper">
                <?php the_post_thumbnail('medium', array('class' => 'insight__image')); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="insight__content">
        <h3 class="insight__title">
            <?php the_title(); ?>
        </h3>
        <p class="insight__description">
            <?php the_excerpt(); ?>
        </p>
        <div class="insight__link-wrapper">
            <a href="<?php the_permalink(); ?>" class="insight__link">Read more</a>
        </div>
    </div>
</div>
