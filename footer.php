			<?php if (true): ?>
                <footer class="footer-cta">
                    <div class="footer-cta__container">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="footer-cta__social">
                                    
                                    <?php $social_heading = get_field('social_heading', 'option'); ?>
                                    <?php if ($social_heading): ?>
                                        <h5><?php echo $social_heading; ?></h5>
                                    <?php endif; ?>

                                    <?php $social_text = get_field('social_text', 'option'); ?>
                                    <?php if ($social_text): ?>
                                        <?php echo $social_text; ?>
                                    <?php endif; ?>

                                    <?php if (have_rows('social_links', 'option')): ?>
                                        <div class="footer-cta__social-links">
                                            <ul>
                                                <?php while (have_rows('social_links', 'option')): the_row(); ?>
                                                    <li class="<?php the_sub_field('icon'); ?>">
                                                        <?php $social_link = get_sub_field('link'); ?>
                                                        <a href="<?php echo esc_url($social_link['url']); ?>" target="<?php echo esc_attr($social_link['target'] ?: '_self'); ?>"><?php echo esc_html($social_link['title']); ?></a>
                                                    </li>
                                                <?php endwhile; ?>
                                            </ul>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="footer-cta__contact">
                                    
                                    <?php $contact_heading = get_field('contact_heading', 'option'); ?>
                                    <?php if ($contact_heading): ?>
                                        <h5><?php echo $contact_heading; ?></h5>
                                    <?php endif; ?>

                                    <?php $contact_text = get_field('contact_text', 'option'); ?>
                                    <?php if ($contact_text): ?>
                                        <?php echo $contact_text; ?>
                                    <?php endif; ?>

                                    <?php if (have_rows('contact_links', 'option')): ?>
                                        <?php while (have_rows('contact_links', 'option')): the_row(); ?>
                                            <div class="footer-cta__link-wrapper">
                                                <?php $contact_link = get_sub_field('link'); ?>
                                                <a href="<?php echo esc_url($contact_link['url']); ?>" class="footer-cta__link footer-cta__link--<?php the_sub_field('icon'); ?>" target="<?php echo esc_attr($contact_link['target'] ?: '_self'); ?>"><?php echo esc_html($contact_link['title']); ?></a>
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>

                    </div>
                </footer>
            <?php endif; ?>

            <footer class="footer" role="contentinfo" itemscope itemtype="https://schema.org/WPFooter">
                <div class="footer__container">

                    <div class="row">
                        <div class="col-md-5 order-md-3 text-center text-md-right">
                            <nav class="footer__links">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Footer Links', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'footer-links',
                                )); ?>
                            </nav>
                        </div>
                        <div class="col-md-5 order-md-1 text-center text-md-left">
                            <div class="footer__copyright">
                                &copy; <?php bloginfo( 'name' ); ?>
                            </div>
                        </div>
                        <div class="col-md-2 order-md-2 text-center">
                            <a href="https://upstart.de/" target="_blank" class="footer__by-upstart">by UPSTART</a>
                        </div>
                    </div>

                </div>
            </footer>

            <!-- Newsletter modal -->
            <div class="modal fade newsletter-modal" id="newsletter-modal" tabindex="-1" role="dialog" aria-labelledby="newsletter-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="newsletter-modal__layout">
                            <div class="newsletter-modal__content-wrapper">
                                <div class="modal-body">

                                    <button type="button" class="newsletter-modal__close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true"><i class="btr bt-times"></i></span>
                                    </button>

                                    <?php $newsletter_modal_heading = get_field('newsletter-modal_heading', 'option'); ?>
                                    <?php if ($newsletter_modal_heading): ?>
                                        <h5 class="newsletter-modal__heading" id="newsletter-modal-label"><?php echo $newsletter_modal_heading; ?></h5>
                                    <?php endif; ?>

                                    <?php the_field('newsletter-modal_text', 'option'); ?>

                                    <form method="POST" action="<?php echo home_url(); ?>/?na=s" class="newsletter-modal__form" onsubmit="return newsletter_check(this)">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="newsletter-modal-name">Name</label>
                                                    <input type="text" class="form-control" id="newsletter-modal-name" placeholder="Name" name="nn" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="sr-only" for="newsletter-modal-email">Email address</label>
                                                    <input type="text" class="form-control" id="newsletter-modal-email" placeholder="Email address" name="ne" required>
                                                </div>
                                            </div>
                                        </div>

                                        <?php the_field('newsletter-modal_text_before_checkbox', 'option'); ?>

                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="newsletter-modal-agree" name="ny" required>
                                            <label class="custom-control-label" for="newsletter-modal-agree"><?php the_field('newsletter-modal_checkbox_text', 'option'); ?></label>
                                        </div>

                                        <?php the_field('newsletter-modal_text_after_checkbox', 'option'); ?>

                                        <div class="newsletter-modal__button-wrapper">
                                            <button type="submit" class="newsletter-modal__button"><?php the_field('newsletter-modal_button_text', 'option'); ?></button>
                                        </div>
                                    </form>

                                </div>
                            </div>

                            <?php $newsletter_modal_image = get_field('newsletter-modal_image', 'option'); ?>
                            <?php if ($newsletter_modal_image): ?>
                                <div class="newsletter-modal__image-wrapper">
                                    <?php echo wp_get_attachment_image($newsletter_modal_image['id'], 'full', false, array('class' => 'newsletter-modal__image')); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cookie notice -->
            <div class="cookie-notice cookie-notice--hidden">
                <div class="cookie-notice__container">

                    <div class="cookie-notice__text-wrapper">

                        <?php $cookie_notice_text = get_field('cookie-notice_text', 'option'); ?>
                        <?php if ($cookie_notice_text): ?>
                            <p class="cookie-notice__text"><?php echo $cookie_notice_text; ?></p>
                        <?php endif; ?>

                        <?php $cookie_notice_decline_button_text = get_field('cookie-notice_decline_button_text', 'option'); ?>
                        <?php if ($cookie_notice_decline_button_text): ?>
                            <div class="cookie-notice__link-wrapper">
                                <a href="" class="cookie-notice__link js-cookie-notice-decline-button"><?php echo $cookie_notice_decline_button_text; ?></a>
                            </div>
                        <?php endif; ?>

                    </div>

                    <div class="cookie-notice__button-wrapper">
                        <button type="button" class="cookie-notice__button js-cookie-notice-accept-button"><?php echo get_field('cookie-notice_accept_button_text', 'option') ?: 'Okay'; ?></button>
                    </div>

                </div>
            </div>

		</div>

		<?php // all js scripts are loaded in library/functions.php ?>
		<?php wp_footer(); ?>

	</body>
</html>
