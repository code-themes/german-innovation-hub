<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>

        <?php /**
         * updated with non-blocking order
         * see here: https://csswizardry.com/2018/11/css-and-network-performance/
         * 
         * In short, place any js here that doesn't need to act on css before any css to
         * speed up page loads.
         */
        ?>

        <?php // drop Google Analytics here ?>
        <?php // end analytics ?>

        <?php // See everything you need to know about the <head> here: https://github.com/joshbuchea/HEAD ?>
        <meta charset='<?php bloginfo( 'charset' ); ?>'>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php // favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
        <link rel="icon" href="<?php echo get_theme_file_uri(); ?>/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo get_theme_file_uri(); ?>/favicon.ico">
        <![endif]-->

        <!-- Apple Touch Icon -->
        <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri(); ?>/library/images/apple-touch-icon.png">

        <!-- Safari Pinned Tab Icon -->
        <link rel="mask-icon" href="<?php echo get_theme_file_uri(); ?>/library/images/icon.svg" color="#0088cc">

        <?php // updated pingback. Thanks @HardeepAsrani https://github.com/HardeepAsrani ?>
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php endif; ?>

        <?php // put font scripts like Typekit/Adobe Fonts here ?>
        <?php // end fonts ?>

        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

    </head>
	<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

		<div id="page">

            <div class="top-bar">
                <div class="top-bar__container">

                    <div class="row">
                        <div class="d-none d-md-block col-md-6">

                            <?php $header_claim = get_field('header_claim', 'option'); ?>
                            <?php if (!empty($header_claim)): ?>
                                <div class="top-bar__text">
                                    <?php echo $header_claim; ?>
                                </div>
                            <?php endif; ?>

                        </div>
                        <div class="col-md-6">

                            <nav class="top-bar-menu top-bar__top-bar-menu">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Top Bar', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'top-bar',
                                )); ?>
                            </nav>
                            
                        </div>
                    </div>
                    
                </div>
            </div>

            <header class="header" role="banner" itemscope itemtype="https://schema.org/WPHeader">
                <div class="header__container">
                    <div class="header__logo-wrapper" itemscope itemtype="https://schema.org/Organization">
                        <a href="<?php echo home_url(); ?>" class="logo header__logo" rel="nofollow" itemprop="url"><?php bloginfo('name'); ?></a>
                    </div>

                    <nav class="main-menu header__main-menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
                        <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                        <?php wp_nav_menu(array(
                            'menu' => __('Main Menu', 'platetheme'),
                            'menu_class' => '',
                            'container' => false,
                            'container_class' => '',
                            'depth' => 2,
                            'theme_location' => 'main-nav',
                            'walker' => new Plate_Sublevel_Walker()
                        )); ?>
                    </nav>
                </div>
            </header>

            <div class="mobile-header mobile-header--hidden">
                <div class="mobile-header__container">

                    <a href="<?php echo home_url(); ?>" class="logo mobile-header__logo"><?php bloginfo('name'); ?></a>

                    <a href="#" class="burger-menu mobile-header__burger-menu js-mobile-menu-open-button">
                        <span class="burger-menu__line"></span>
                        <span class="burger-menu__line"></span>
                        <span class="burger-menu__line"></span>
                    </a>

                </div>
            </div>

            <div class="mobile-menu-wrapper mobile-menu-wrapper--hidden">
                <a href="#" class="burger-menu-close mobile-menu-wrapper__burger-menu-close js-mobile-menu-close-button">
                    <span class="burger-menu-close__line"></span>
                    <span class="burger-menu-close__line"></span>
                    <span class="burger-menu-close__line"></span>
                </a>
                <div class="mobile-menu-wrapper__layout">
                    <div class="mobile-menu-wrapper__left">
                        <div class="mobile-menu-wrapper__left-container">
                            
                            <a href="<?php echo home_url(); ?>" class="logo logo--white mobile-menu-wrapper__logo"><?php bloginfo('name'); ?></a>

                            <?php $mobile_menu_claim = get_field('mobile-menu_claim', 'option'); ?>
                            <?php if ($mobile_menu_claim): ?>
                                <?php $mobile_menu_claim = explode("\n", $mobile_menu_claim); ?>
                                <div class="mobile-menu-wrapper__claim-wrapper">
                                    <h3 class="mobile-menu-wrapper__claim">
                                        <?php $i = 0; foreach ($mobile_menu_claim as $line): ?>
                                            <?php echo $i > 0 ? '<br>' : ''; ?>
                                            <span><?php echo $line; ?></span>
                                        <?php $i++; endforeach; ?>
                                    </h3>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="mobile-menu-wrapper__right">
                        <div class="mobile-menu-wrapper__right-container">

                            <nav class="mobile-menu mobile-menu-wrapper__mobile-menu">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Mobile Menu', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'mobile-nav',
                                )); ?>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
