<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'plan-details';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$intro = get_field('intro');
$accordion = get_field('accordion');
$benefits = get_field('benefits');
$partners = get_field('partners');
$cta = get_field('cta');

?>
<div id="<?php echo esc_attr($id); ?>_wrapper" class="<?php echo esc_attr($className); ?>">
    <div class="plan-details__container">

        <!-- Intro -->
        <?php if ($intro && !empty($intro['heading'])): ?>
            <div class="plan-details__intro" id="<?php echo esc_attr($id); ?>">
                <div class="row justify-content-center">
                    <div class="col-md-10">

                        <div class="plan-details-intro">

                            <?php if (!empty($intro['carousel']) && count($intro['carousel'])): ?>
                                <div class="plan-details-intro__carousel-wrapper">
                                    <!-- Carousel -->
                                    <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php $i = 0; foreach ($intro['carousel'] as $image): ?>
                                                <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                                                    <?php echo wp_get_attachment_image($image['id'], 'plate-image-350-auto', false, array('class' => 'd-block img-fluid')); ?>
                                                </div>
                                            <?php $i++; endforeach; ?>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <?php $i = 0; foreach ($intro['carousel'] as $image): ?>
                                                <li data-target="#<?php echo esc_attr($id); ?>_carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
                                            <?php $i++; endforeach; ?>
                                        </ol>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="plan-details-intro__content">

                                <?php if (!empty($intro['heading'])): ?>
                                    <h2 class="plan-details-intro__heading"><?php echo $intro['heading']; ?></h2>
                                <?php endif; ?>

                                <?php if (!empty($intro['text'])): ?>
                                    <p class="plan-details-intro__text"><?php echo $intro['text']; ?></p>
                                <?php endif; ?>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Accordion -->
        <?php if ($accordion && !empty($accordion['accordion']) && count($accordion['accordion'])): ?>
            <div class="plan-details__accordion">
                <div class="row justify-content-center">
                    <div class="col-md-10">

                        <div class="accordion">
                            <div class="accordion__col">
                                <?php $i = 0; foreach ($accordion['accordion'] as $item): ?>
                                    <?php $i++; ?>
                                    <?php if ($i % 2 == 0): continue; endif; ?>
                                    <div class="accordion__item">
                                        <div class="accordion__link-wrapper">
                                            <a href="" class="accordion__link js-accordion-link">
                                                <?php echo $item['title']; ?>
                                                <span class="accordion__link-icon"></span>
                                            </a>
                                        </div>
                                        <div class="accordion__content">
                                            <?php echo $item['text']; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="accordion__col">
                                <?php $i = 0; foreach ($accordion['accordion'] as $item): ?>
                                    <?php $i++; ?>
                                    <?php if ($i % 2 != 0): continue; endif; ?>
                                    <div class="accordion__item">
                                        <div class="accordion__link-wrapper">
                                            <a href="" class="accordion__link js-accordion-link">
                                                <?php echo $item['title']; ?>
                                                <span class="accordion__link-icon"></span>
                                            </a>
                                        </div>
                                        <div class="accordion__content">
                                            <?php echo $item['text']; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Benefits -->
        <?php if ($benefits && !empty($benefits['benefits']) && count($benefits['benefits'])): ?>
            <div class="plan-details__benefits">
                <div class="row justify-content-center">
                    <div class="col-md-5">

                        <?php $i = 0; foreach ($benefits['benefits'] as $item): ?>
                            <?php $i++; ?>
                            <?php if ($i % 2 == 0): continue; endif; ?>
                            <div class="plan-details-benefit">
                                <h4 class="plan-details-benefit__heading"><?php echo $item['heading']; ?></h4>
                                <p class="plan-details-benefit__text"><?php echo $item['text']; ?></p>
                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="col-md-5">

                        <?php $i = 0; foreach ($benefits['benefits'] as $item): ?>
                            <?php $i++; ?>
                            <?php if ($i % 2 != 0): continue; endif; ?>
                            <div class="plan-details-benefit">
                                <h4 class="plan-details-benefit__heading"><?php echo $item['heading']; ?></h4>
                                <p class="plan-details-benefit__text"><?php echo $item['text']; ?></p>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Partners -->
        <?php if ($partners && !empty($partners['items']) && count($partners['items'])): ?>
            <div class="plan-details__partners">
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <div class="plan-details-partners">

                            <?php if (!empty($partners['heading'])): ?>
                                <h3 class="plan-details-partners__heading"><?php echo $partners['heading']; ?></h3>
                            <?php endif; ?>

                            <!-- Logos -->
                            <div class="plan-details-partners__logos">
                                <div class="row justify-content-center">
                                    <?php $i = 0; foreach ($partners['items'] as $item): ?>

                                        <?php if ($i != 0 && $i % 4 == 0): ?>
                                            </div>
                                            <div class="row justify-content-center">
                                        <?php endif; ?>

                                        <div class="col-6 col-md-3">
                                            <?php $image = $item['logo']; ?>
                                            <?php $image_retina = $item['logo_retina']; ?>
                                            <?php $image_width = get_sub_field('logo_width'); ?>
                                            <?php $image_height = get_sub_field('logo_height'); ?>
                                            <?php if (!empty($image)): ?>
                                                <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="plan-details-partners__logo" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
                                            <?php endif; ?>

                                        </div>

                                    <?php $i++; endforeach; ?>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- CTA -->
        <?php if ($cta && !empty($cta['button']['title'])): ?>
            <div class="plan-details__cta">
                <div class="plan-details-cta">

                    <?php if (!empty($cta['heading'])): ?>
                        <h3 class="plan-details-cta__heading"><?php echo $cta['heading']; ?></h3>
                    <?php endif; ?>

                    <?php $link = $cta['button']; ?>
                    <?php if (!empty($link['title'])): ?>
                        <div class="plan-details-cta__button-wrapper">
                            <a href="<?php echo esc_url($link['url']); ?>" class="btn btn-lg btn-outline-warning plan-details-cta__button" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
