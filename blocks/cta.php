<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading') ?: '';
$link = get_field('button') ?: array(
    'url' => '',
    'title' => '',
    'target' => '_self'
);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="cta__container">

        <div class="cta__box">

            <?php if (!empty($heading)): ?>
                <div class="cta__heading-wrapper">
                    <h3 class="cta__heading"><?php echo $heading; ?></h3>
                </div>
            <?php endif; ?>

            <?php if (!empty($link['title'])): ?>
                <div class="cta__button-wrapper">
                    <a href="<?php echo esc_url($link['url']); ?>" class="btn btn-lg btn-outline-warning cta__button" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>
