<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'teaser';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$text = get_field('text') ?: '';
$link = get_field('button') ?: array(
    'url' => '',
    'title' => '',
    'target' => '_self'
);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="teaser__container">

        <div class="row justify-content-center">
            <div class="col-md-8">

                <?php if (!empty($text)): ?>
                    <div class="teaser__content">
                        <?php echo $text; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($link['title'])): ?>
                    <div class="teaser__cta-wrapper">
                        <a href="<?php echo esc_url($link['url']); ?>" class="btn btn-outline-primary teaser__cta" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>
</div>
