<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'network';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading') ?: '';
$subheading = get_field('subheading') ?: '';
$link = get_field('button') ?: array(
    'url' => '',
    'title' => '',
    'target' => '_self'
);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="network__container">

        <div class="network__header">

            <?php if (!empty($heading)): ?>
                <h2 class="network__heading"><?php echo $heading; ?></h2>
            <?php endif; ?>

            <?php if (!empty($subheading)): ?>
                <p class="network__subheading"><?php echo $subheading; ?></p>
            <?php endif; ?>

        </div>

        <?php if (have_rows('items')): ?>
            <div class="network__logos">
                <div class="row">
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        
                        <?php if ($i != 0 && $i % 4 == 0): ?>
                            </div>
                            <div class="row">
                        <?php endif; ?>

                        <div class="col-6 col-md-3">

                            <?php $image = get_sub_field('logo'); ?>
                            <?php $image_retina = get_sub_field('logo_retina'); ?>
                            <?php $image_width = get_sub_field('logo_width'); ?>
                            <?php $image_height = get_sub_field('logo_height'); ?>
                            <?php if (!empty($image)): ?>
                                <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="network__logo" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
                            <?php endif; ?>

                        </div>

                    <?php $i++; endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($link['title'])): ?>
            <div class="network__button-wrapper">
                <a href="<?php echo esc_url($link['url']); ?>" class="btn btn-outline-primary network__button" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
            </div>
        <?php endif; ?>

    </div>
</div>
