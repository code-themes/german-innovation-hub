<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'events';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$eventbrite_organization_id = get_field('eventbrite_organization_id');
$eventbrite_path = !empty($eventbrite_organization_id) ? '/organizations/'.$eventbrite_organization_id.'/events/' : '/users/me/events/';
$eventbrite = get_eventbrite_events($eventbrite_path);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="events__container">

        <?php if (!empty($eventbrite->events)): ?>
            <div class="row">
                <?php $i = 0; foreach ($eventbrite->events as $event): ?>

                    <?php if ($i != 0 && $i % 2 == 0): ?>
                        </div>
                        <div class="row">
                    <?php endif; ?>

                    <div class="col-md-6">

                        <div class="event">
                            <?php if (!empty($event->logo->url)): ?>
                                <div class="event__media">
                                    <div class="event__image-wrapper">
                                        <img src="<?php echo $event->logo->url; ?>" alt="Event" class="event__image">
                                    </div>
                                    <?php if (!empty($event->category->short_name)): ?>
                                        <div class="event__label-wrapper">
                                            <span class="event__label">
                                                <?php echo $event->category->short_name; ?>
                                            </span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="event__content">

                                <?php if (!empty($event->start->local)): ?>
                                    <p class="event__date">
                                        <?php
                                            $date = $event->start->local;
                                            $date_time = DateTime::createFromFormat('Y-m-d\TH:i:s', $date);
                                            echo $date_time->format('d.m.Y');
                                        ?>
                                    </p>
                                <?php endif; ?>

                                <?php if (!empty($event->name->text)): ?>
                                    <h3 class="event__title">
                                        <?php echo $event->name->text; ?>
                                    </h3>
                                <?php endif; ?>

                                <?php if (!empty($event->venue->name)): ?>
                                    <p class="event__location">
                                        <?php echo $event->venue->name; ?>
                                    </p>
                                <?php endif; ?>

                                <?php if (!empty($event->description->text)): ?>
                                    <p class="event__description">
                                        <?php echo trim(substr($event->description->text, 0, 60)); ?>...
                                    </p>
                                <?php endif; ?>

                                <?php if (!empty($event->url)): ?>
                                    <div class="event__link-wrapper">
                                        <a href="<?php echo $event->url; ?>" target="_blank" class="event__link">Go to Eventbrite</a>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>

                    </div>
                <?php $i++; endforeach; ?>
            </div>
        <?php else: ?>
            <div class="text-center">No events found.</div>
        <?php endif; ?>

    </div>
</div>
