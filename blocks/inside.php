<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'inside';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$posts = get_field('posts');
$follow_us = get_field('follow_us');

// Medium
$medium_username = $posts['medium_username'];
$medium = get_medium_articles($medium_username);

// Blog
$wp_query = new WP_Query(array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 2
));

// Eventbrite
$eventbrite_organization_id = $posts['eventbrite_organization_id'];
$eventbrite_path = !empty($eventbrite_organization_id) ? '/organizations/'.$eventbrite_organization_id.'/events/' : '/users/me/events/';
$eventbrite = get_eventbrite_events($eventbrite_path);

// Twitter
$twitter_handle1 = $posts['twitter_handle'];
$twitter_handle2 = $twitter_handle1;

$tweets = get_twitter_tweets($twitter_handle1);

if (!empty($tweets)) {
    $tweets_length = floor(count($tweets) / 2);
    $tweets1 = array_slice($tweets, 0, $tweets_length);
    $tweets2 = array_slice($tweets, $tweets_length, $tweets_length);
}

// Instagram
$instagram = get_instagram_images();

// Facebook
$facebook_username = $posts['facebook_username'];
$facebook = get_facebook_posts();

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="inside__container">

        <div class="row">
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($instagram->data[0])): ?>
                            <div class="inside-instagram">
                                <a href="<?php echo $instagram->data[0]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                    <img src="<?php echo $instagram->data[0]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($follow_us['instagram_link'])): ?>
                            <a href="<?php echo esc_url($follow_us['instagram_link']['url']); ?>" target="<?php echo esc_attr($follow_us['instagram_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--instagram">
                                <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['instagram_link']['title'])); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="inside__tile inside__tile--ratio-2-1">
                    <div class="inside__tile-inner">

                        <?php if (!empty($twitter_handle1)): ?>
                            <div class="inside-twitter">
                                <div class="inside-twitter__header">
                                    <a href="https://twitter.com/<?php echo $twitter_handle1; ?>" target="_blank" class="inside-twitter__link">
                                        <span class="inside-twitter__icon">Twitter</span>
                                        <span class="inside-twitter__handle">@<?php echo $twitter_handle1; ?></span>
                                    </a>
                                    <?php if (!empty($tweets1[0]->created_at)): ?>
                                        <span class="inside-twitter__date">
                                            <?php
                                                $date_time = new DateTime($tweets1[0]->created_at);
                                                echo $date_time->format('d.m.Y');
                                            ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <?php if (!empty($tweets1)): ?>
                                    <div class="inside-twitter__carousel">
                                        <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                                            <div class="carousel-inner">
                                                <?php $i = 0; foreach($tweets1 as $tweet): ?>
                                                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                                                        <div class="carousel-image-placeholder"></div>
                                                        <div class="carousel-caption">

                                                            <div class="inside-twitter__content">
                                                                <p class="inside-twitter__tweet">
                                                                    <?php echo $tweet->text; ?>
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                <?php $i++; endforeach; ?>
                                            </div>
                                            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <ol class="carousel-indicators">
                                                <?php $i = 0; foreach($tweets1 as $tweet): ?>
                                                    <li data-target="#<?php echo esc_attr($id); ?>_carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
                                                <?php $i++; endforeach; ?>
                                            </ol>
                                            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                
                <div class="inside__tile inside__tile--ratio-2-1">
                    <div class="inside__tile-inner">
                        
                        <?php if ($wp_query->have_posts()): ?>
                            <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                                <div class="inside-article">
                                    <div class="inside-article__inner">

                                        <?php if (has_post_thumbnail()): ?>
                                            <div class="inside-article__media">
                                                <div class="inside-article__image-wrapper">
                                                    <?php the_post_thumbnail('medium', array('class' => 'inside-article__image')); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="inside-article__content">
                                            <h3 class="inside-article__title">
                                                <?php the_title(); ?>
                                            </h3>
                                            <p class="inside-article__description">
                                                <?php the_excerpt(); ?>
                                            </p>
                                            <div class="inside-article__link-wrapper">
                                                <a href="<?php the_permalink(); ?>" class="inside-article__link">Read more</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php break; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-6">

                        <div class="inside__tile">
                            <div class="inside__tile-inner">

                                <?php if (!empty($instagram->data[1])): ?>
                                    <div class="inside-instagram">
                                        <a href="<?php echo $instagram->data[1]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                            <img src="<?php echo $instagram->data[1]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                        </a>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>

                    </div>
                    <div class="col-6">

                        <div class="inside__tile">
                            <div class="inside__tile-inner">

                                <?php if (!empty($follow_us['meetup_link'])): ?>
                                    <a href="<?php echo esc_url($follow_us['meetup_link']['url']); ?>" target="<?php echo esc_attr($follow_us['meetup_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--meetup">
                                        <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['meetup_link']['title'])); ?></span>
                                    </a>
                                <?php endif; ?>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($instagram->data[2])): ?>
                            <div class="inside-instagram">
                                <a href="<?php echo $instagram->data[2]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                    <img src="<?php echo $instagram->data[2]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($follow_us['medium_link'])): ?>
                            <a href="<?php echo esc_url($follow_us['medium_link']['url']); ?>" target="<?php echo esc_attr($follow_us['medium_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--medium">
                                <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['medium_link']['title'])); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-md-9">

                <div class="inside__tile inside__tile--ratio-3-1">
                    <div class="inside__tile-inner">
                        
                        <?php if (!empty($facebook[0])): ?>
                            <div class="inside-facebook">
                                <div class="inside-facebook__header">
                                    <a href="" class="inside-facebook__link">
                                        <span class="inside-facebook__icon">Facebook</span>
                                        <span class="inside-facebook__username">@<?php echo $facebook_username; ?></span>
                                    </a>
                                    <span class="inside-facebook__date">
                                        <?php echo date('d.m.Y', strtotime($facebook[0]->created_time)); ?>
                                    </span>
                                </div>
                                <div class="inside-facebook__content">
                                    <div class="inside-facebook__text-wrapper">
                                        <?php if (!empty($facebook[0]->message)): ?>
                                            <p class="inside-facebook__text">
                                                <?php echo $facebook[0]->message; ?>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                    <?php /*<div class="inside-facebook__image-wrapper">
                                        <img srcset="<?php echo get_theme_file_uri(); ?>/library/assets/images/inside/facebook.jpg 1x, <?php echo get_theme_file_uri(); ?>/library/assets/images/inside/facebook@2x.jpg 2x" src="<?php echo get_theme_file_uri(); ?>/library/assets/images/inside/facebook.jpg" alt="Image" class="inside-facebook__image">
                                    </div>*/ ?>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($instagram->data[3])): ?>
                            <div class="inside-instagram">
                                <a href="<?php echo $instagram->data[3]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                    <img src="<?php echo $instagram->data[3]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($follow_us['facebook_link'])): ?>
                            <a href="<?php echo esc_url($follow_us['facebook_link']['url']); ?>" target="<?php echo esc_attr($follow_us['facebook_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--facebook">
                                <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['facebook_link']['title'])); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="inside__tile inside__tile--ratio-2-1">
                    <div class="inside__tile-inner">

                        <?php if (!empty($eventbrite->events[0])): ?>
                            <a href="<?php echo $eventbrite->events[0]->url; ?>" target="_blank" class="inside-event">

                                <?php if (!empty($eventbrite->events[0]->logo->url)): ?>
                                    <div class="inside-event__image-wrapper">
                                        <img src="<?php echo $eventbrite->events[0]->logo->url; ?>" alt="Image" class="inside-event__image">
                                    </div>
                                <?php endif; ?>

                                <div class="inside-event__content">

                                    <?php if (!empty($eventbrite->events[0]->start->local)): ?>
                                        <div class="inside-event__date-wrapper">
                                            <p class="inside-event__date">
                                                <?php
                                                    $date = $eventbrite->events[0]->start->local;
                                                    $date_time = DateTime::createFromFormat('Y-m-d\TH:i:s', $date);
                                                    echo $date_time->format('M');
                                                    echo '<br>';
                                                    echo $date_time->format('d');
                                                ?>
                                            </p>
                                        </div>
                                    <?php endif; ?>

                                    <div class="inside-event__title-wrapper">

                                        <?php if (!empty($eventbrite->events[0]->name->text)): ?>
                                            <h3 class="inside-event__title">
                                                <?php echo $eventbrite->events[0]->name->text; ?>
                                            </h3>
                                        <?php endif; ?>

                                        <?php if (!empty($eventbrite->events[0]->venue->name)): ?>
                                            <p class="inside-event__subtitle">
                                                <?php echo $eventbrite->events[0]->venue->name; ?>
                                            </p>
                                        <?php endif; ?>

                                    </div>
                                </div>

                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div class="inside__tile inside__tile--ratio-2-1">
                    <div class="inside__tile-inner">
                        
                        <?php if (!empty($facebook[1])): ?>
                            <div class="inside-facebook">
                                <div class="inside-facebook__header">
                                    <a href="" class="inside-facebook__link">
                                        <span class="inside-facebook__icon">Facebook</span>
                                        <span class="inside-facebook__username">@<?php echo $facebook_username; ?></span>
                                    </a>
                                    <span class="inside-facebook__date"><?php echo date('d.m.Y', strtotime($facebook[1]->created_time)); ?></span>
                                </div>
                                <div class="inside-facebook__content">
                                    <div class="inside-facebook__text-wrapper">
                                        <?php if (!empty($facebook[1]->message)): ?>
                                            <p class="inside-facebook__text">
                                                <?php echo $facebook[1]->message; ?>
                                            </p>
                                        <?php endif; ?>
                                        <?php /*<div class="inside-facebook__addon">
                                            <div class="inside-facebook__addon-link-wrapper">
                                                <a href="" class="inside-facebook__addon-link">http://kickupyourdesk.com/</a>
                                            </div>
                                            <p class="inside-facebook__addon-description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                        </div>*/ ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($instagram->data[4])): ?>
                            <div class="inside-instagram">
                                <a href="<?php echo $instagram->data[4]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                    <img src="<?php echo $instagram->data[4]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($follow_us['linkedin_link'])): ?>
                            <a href="<?php echo esc_url($follow_us['linkedin_link']['url']); ?>" target="<?php echo esc_attr($follow_us['linkedin_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--linkedin">
                                <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['linkedin_link']['title'])); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div class="inside__tile inside__tile--ratio-2-1">
                    <div class="inside__tile-inner">
                        
                        <?php if ($wp_query->have_posts()): ?>
                            <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                                <div class="inside-article">
                                    <div class="inside-article__inner">

                                        <?php if (has_post_thumbnail()): ?>
                                            <div class="inside-article__media">
                                                <div class="inside-article__image-wrapper">
                                                    <?php the_post_thumbnail('medium', array('class' => 'inside-article__image')); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="inside-article__content">
                                            <h3 class="inside-article__title">
                                                <?php the_title(); ?>
                                            </h3>
                                            <p class="inside-article__description">
                                                <?php the_excerpt(); ?>
                                            </p>
                                            <div class="inside-article__link-wrapper">
                                                <a href="<?php the_permalink(); ?>" class="inside-article__link">Read more</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php break; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($follow_us['eventbrite_link'])): ?>
                            <a href="<?php echo esc_url($follow_us['eventbrite_link']['url']); ?>" target="<?php echo esc_attr($follow_us['eventbrite_link']['target'] ?: '_self'); ?>" class="inside-follow inside-follow--eventbrite">
                                <span class="inside-follow__text"><?php echo str_replace(array('&lt;br&gt;'), array('<br>'), esc_html($follow_us['eventbrite_link']['title'])); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-6 col-md-3">

                <div class="inside__tile">
                    <div class="inside__tile-inner">

                        <?php if (!empty($instagram->data[5])): ?>
                            <div class="inside-instagram">
                                <a href="<?php echo $instagram->data[5]->permalink; ?>" target="_blank" class="inside-instagram__link">
                                    <img src="<?php echo $instagram->data[5]->media_url; ?>" alt="Image" class="inside-instagram__image">
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-4">

                <div class="inside__tile inside__tile--ratio-md-custom">
                    <div class="inside__tile-inner">
                        
                        <?php if (!empty($facebook[2])): ?>
                            <div class="inside-facebook">
                                <div class="inside-facebook__header">
                                    <a href="" class="inside-facebook__link">
                                        <span class="inside-facebook__icon">Facebook</span>
                                        <span class="inside-facebook__username">@<?php echo $facebook_username; ?></span>
                                    </a>
                                    <span class="inside-facebook__date"><?php echo date('d.m.Y', strtotime($facebook[2]->created_time)); ?></span>
                                </div>
                                <div class="inside-facebook__content">
                                    <div class="inside-facebook__text-wrapper">
                                        <?php if (!empty($facebook[2]->message)): ?>
                                            <p class="inside-facebook__text">
                                                <?php echo $facebook[2]->message; ?>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-md-4">

                <div class="inside__tile inside__tile--ratio-md-custom">
                    <div class="inside__tile-inner">

                        <?php if (!empty($eventbrite->events[1])): ?>
                            <a href="<?php echo $eventbrite->events[1]->url; ?>" target="_blank" class="inside-event">

                                <?php if (!empty($eventbrite->events[1]->logo->url)): ?>
                                    <div class="inside-event__image-wrapper">
                                        <img src="<?php echo $eventbrite->events[1]->logo->url; ?>" alt="Image" class="inside-event__image">
                                    </div>
                                <?php endif; ?>

                                <div class="inside-event__content">

                                    <?php if (!empty($eventbrite->events[1]->start->local)): ?>
                                        <div class="inside-event__date-wrapper inside-event__date-wrapper--red">
                                            <p class="inside-event__date">
                                                <?php
                                                    $date = $eventbrite->events[1]->start->local;
                                                    $date_time = DateTime::createFromFormat('Y-m-d\TH:i:s', $date);
                                                    echo $date_time->format('M');
                                                    echo '<br>';
                                                    echo $date_time->format('d');
                                                ?>
                                            </p>
                                        </div>
                                    <?php endif; ?>

                                    <div class="inside-event__title-wrapper">

                                        <?php if (!empty($eventbrite->events[1]->name->text)): ?>
                                            <h3 class="inside-event__title">
                                                <?php echo $eventbrite->events[1]->name->text; ?>
                                            </h3>
                                        <?php endif; ?>

                                        <?php if (!empty($eventbrite->events[1]->venue->name)): ?>
                                            <p class="inside-event__subtitle">
                                                <?php echo $eventbrite->events[1]->venue->name; ?>
                                            </p>
                                        <?php endif; ?>

                                    </div>
                                </div>

                            </a>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="col-md-4">

                <div class="inside__tile inside__tile--ratio-md-custom">
                    <div class="inside__tile-inner">
                        
                        <?php if (!empty($twitter_handle2)): ?>
                            <div class="inside-twitter">
                                <div class="inside-twitter__header">
                                    <a href="https://twitter.com/<?php echo $twitter_handle2; ?>" target="_blank" class="inside-twitter__link">
                                        <span class="inside-twitter__icon">Twitter</span>
                                        <span class="inside-twitter__handle">@<?php echo $twitter_handle2; ?></span>
                                    </a>
                                    <?php if (!empty($tweets2[0]->created_at)): ?>
                                        <span class="inside-twitter__date">
                                            <?php
                                                $date_time = new DateTime($tweets2[0]->created_at);
                                                echo $date_time->format('d.m.Y');
                                            ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <?php if (!empty($tweets2)): ?>
                                    <div class="inside-twitter__carousel">
                                        <div id="<?php echo esc_attr($id); ?>_carousel2" class="carousel slide" data-ride="carousel" data-interval="false">
                                            <div class="carousel-inner">
                                                <?php $i = 0; foreach($tweets2 as $tweet): ?>
                                                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                                                        <div class="carousel-image-placeholder"></div>
                                                        <div class="carousel-caption">

                                                            <div class="inside-twitter__content">
                                                                <p class="inside-twitter__tweet">
                                                                    <?php echo $tweet->text; ?>
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                <?php $i++; endforeach; ?>
                                            </div>
                                            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel2" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <ol class="carousel-indicators">
                                                <?php $i = 0; foreach($tweets2 as $tweet): ?>
                                                    <li data-target="#<?php echo esc_attr($id); ?>_carousel2" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
                                                <?php $i++; endforeach; ?>
                                            </ol>
                                            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel2" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<?php wp_reset_postdata(); ?>
