<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'testimonials';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
// ...

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel">

        <?php $i = 0; ?>
        <?php if (have_rows('quotes')): ?>
            <div class="carousel-inner">
                <?php $i = 0; while (have_rows('quotes')): the_row(); ?>
                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                        <div class="carousel-image-placeholder"></div>
                        <div class="carousel-caption">

                            <div class="testimonials__container">
                                <div class="row justify-content-center">
                                    <div class="col-md-11">
                                        <blockquote class="blockquote">
                                            
                                            <?php $quote = get_sub_field('quote'); ?>
                                            <?php if (!empty($quote)): ?>
                                                <p><?php echo $quote; ?></p>
                                            <?php endif; ?>

                                            <?php $source = get_sub_field('source'); ?>
                                            <?php if (!empty($source)): ?>
                                                <footer class="blockquote-footer">
                                                    <?php echo $source; ?>
                                                </footer>
                                            <?php endif; ?>

                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if ($i > 1): ?>
            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php endif; ?>
    </div>

</div>
