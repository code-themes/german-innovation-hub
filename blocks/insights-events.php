<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'insights-events';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$insights_heading = get_field('insights_heading') ?: '';
$events_heading = get_field('events_heading') ?: '';
$insights_subheading = get_field('insights_subheading') ?: '';
$events_subheading = get_field('events_subheading') ?: '';
$insights_link = get_field('insights_button') ?: array(
    'url' => '',
    'title' => '',
    'target' => '_self'
);
$events_link = get_field('events_button') ?: array(
    'url' => '',
    'title' => '',
    'target' => '_self'
);

// Posts
$posts = get_field('posts');

// Medium
$medium_username = get_field('medium_username');
$medium = get_medium_articles($medium_username);

// Eventbrite
$eventbrite_organization_id = get_field('eventbrite_organization_id');
$eventbrite_path = !empty($eventbrite_organization_id) ? '/organizations/'.$eventbrite_organization_id.'/events/' : '/users/me/events/';
$eventbrite = get_eventbrite_events($eventbrite_path);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="insights-events__container">

        <div class="row justify-content-center">
            <div class="col-md-6">

                <div class="insights-preview">
                    <div class="insights-preview__header">

                        <?php if (!empty($insights_heading)): ?>
                            <h2 class="insights-preview__heading"><?php echo $insights_heading; ?></h2>
                        <?php endif; ?>

                        <?php if (!empty($insights_heading)): ?>
                            <p class="insights-preview__subheading"><?php echo $insights_subheading; ?></p>
                        <?php endif; ?>

                    </div>

                    <?php if (!empty($posts)): ?>
                        <div class="insights-preview__content">
                            <?php global $post; ?>
                            <?php $i = 0; foreach ($posts as $post): ?>
                            <?php  if ($i >= 2) { break; }  ?>
                            <?php setup_postdata($post); ?>
                                <a href="<?php the_permalink(); ?>" class="insights-preview__item">

                                    <?php if (has_post_thumbnail()): ?>
                                        <span class="insights-preview__item-image-wrapper">
                                            <span class="insights-preview__item-image-box"></span>
                                            <?php the_post_thumbnail('post-thumbnail', array('class' => 'insights-preview__item-image')); ?>
                                        </span>
                                    <?php endif; ?>

                                    <span class="insights-preview__item-content">
                                        <h3 class="insights-preview__item-heading">
                                            <?php the_title(); ?>
                                        </h3>
                                        <p class="insights-preview__item-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                    </span>

                                </a>
                            <?php $i++; endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    <?php else: ?>
                        <div class="mt-5 mb-5">No articles found.</div>
                    <?php endif; ?>

                    <?php /*if (!empty($medium->items)): ?>
                        <div class="insights-preview__content">
                            <?php $i = 0; foreach ($medium->items as $item): ?>
                            <?php  if ($i >= 2) { break; }  ?>
                                <a href="<?php echo $item->link; ?>" target="_blank" class="insights-preview__item">

                                    <?php if (!empty($item->thumbnail)): ?>
                                        <span class="insights-preview__item-image-wrapper">
                                            <span class="insights-preview__item-image-box"></span>
                                            <img src="<?php echo $item->thumbnail; ?>" alt="Insight" class="insights-preview__item-image">
                                        </span>
                                    <?php endif; ?>

                                    <span class="insights-preview__item-content">
                                        <h3 class="insights-preview__item-heading">
                                            <?php echo $item->title; ?>
                                        </h3>
                                        <p class="insights-preview__item-text">
                                            <?php echo trim(substr(strip_tags($item->content), 0, 170)); ?>...
                                        </p>
                                    </span>

                                </a>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php else: ?>
                        <div class="mt-5 mb-5">No articles found.</div>
                    <?php endif;*/ ?>

                    <?php if (!empty($insights_link['title'])): ?>
                        <div class="insights-preview__button-wrapper d-md-none">
                            <a href="<?php echo esc_url($insights_link['url']); ?>" class="btn btn-outline-primary insights-preview__button" target="<?php echo esc_attr($insights_link['target'] ?: '_self'); ?>"><?php echo esc_html($insights_link['title']); ?></a>
                        </div>
                    <?php endif; ?>

                </div>

            </div>
            <div class="col-md-6">

                <div class="events-preview">
                    <div class="events-preview__header">

                        <?php if (!empty($events_heading)): ?>
                            <h2 class="events-preview__heading"><?php echo $events_heading; ?></h2>
                        <?php endif; ?>

                        <?php if (!empty($events_subheading)): ?>
                            <p class="events-preview__subheading"><?php echo $events_subheading; ?></p>
                        <?php endif; ?>

                    </div>

                    <?php if (!empty($eventbrite->events)): ?>
                        <div class="events-preview__content">
                            <?php $i = 0; foreach ($eventbrite->events as $event): ?>
                                <div class="events-preview__item">
                                    <div class="events-preview__item-date-wrapper">

                                        <?php if (!empty($event->start->local)): ?>
                                            <p class="events-preview__item-date">
                                                <?php
                                                    $date = $event->start->local;
                                                    $date_time = DateTime::createFromFormat('Y-m-d\TH:i:s', $date);
                                                    echo $date_time->format('d.m.Y');
                                                ?>
                                            </p>
                                        <?php endif; ?>

                                    </div>
                                    <div class="events-preview__item-content">

                                        <?php if (!empty($event->name->text)): ?>
                                            <h3 class="events-preview__item-heading"><a href="<?php echo $event->url; ?>" target="_blank" class="events-preview__item-heading-link">
                                                <?php echo $event->name->text; ?>
                                            </a></h3>
                                        <?php endif; ?>

                                        <?php if (!empty($event->venue->name)): ?>
                                            <p class="events-preview__item-location">
                                                <?php echo $event->venue->name; ?>
                                            </p>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php else: ?>
                        <div class="mt-5 mb-5">No events found.</div>
                    <?php endif; ?>

                    <?php if (!empty($events_link['title'])): ?>
                        <div class="events-preview__button-wrapper d-md-none">
                            <a href="<?php echo esc_url($events_link['url']); ?>" class="btn btn-outline-primary events-preview__button" target="<?php echo esc_attr($events_link['target'] ?: '_self'); ?>"><?php echo esc_html($events_link['title']); ?></a>
                        </div>
                    <?php endif; ?>

                </div>

            </div>
        </div>

        <div class="d-none d-md-block">
            <div class="row justify-content-center">
                <div class="col-md-6">

                    <?php if (!empty($insights_link['title'])): ?>
                        <div class="insights-preview__button-wrapper">
                            <a href="<?php echo esc_url($insights_link['url']); ?>" class="btn btn-outline-primary insights-preview__button" target="<?php echo esc_attr($insights_link['target'] ?: '_self'); ?>"><?php echo esc_html($insights_link['title']); ?></a>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="col-md-6">

                    <?php if (!empty($events_link['title'])): ?>
                        <div class="events-preview__button-wrapper">
                            <a href="<?php echo esc_url($events_link['url']); ?>" class="btn btn-outline-primary events-preview__button" target="<?php echo esc_attr($events_link['target'] ?: '_self'); ?>"><?php echo esc_html($events_link['title']); ?></a>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>
</div>
