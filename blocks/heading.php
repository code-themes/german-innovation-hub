<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'heading';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading') ?: '';
$subheading = get_field('subheading') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="heading__container">

        <?php if (!empty($heading)): ?>
            <div class="heading__heading-wrapper">
                <h1 class="heading__heading"><?php echo $heading; ?></h1>
            </div>
        <?php endif; ?>

        <?php if (!empty($subheading)): ?>
            <div class="heading__subheading-wrapper">
                <h2 class="heading__subheading"><?php echo $subheading; ?></h2>
            </div>
        <?php endif; ?>

    </div>
</div>
