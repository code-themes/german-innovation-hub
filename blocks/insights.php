<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'insights';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$medium_username = get_field('medium_username') ?: '';
$medium = get_medium_articles($medium_username);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="insights__container">

        <?php if (!empty($medium_username)): ?>
            <?php if (!empty($medium->items)): ?>
                <div class="row justify-content-center">
                    <div class="col-md-8 insights__items">

                        <?php $i = 0; foreach ($medium->items as $item): ?>
                        <?php if ($i >= 4) { break; } ?>
                            <div class="insight">
                                <?php if (!empty($item->thumbnail)): ?>
                                    <div class="insight__media">
                                        <div class="insight__image-wrapper">
                                            <img src="<?php echo $item->thumbnail; ?>" alt="Insight" class="insight__image">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="insight__content">
                                    <h3 class="insight__title">
                                        <?php echo $item->title; ?>
                                    </h3>
                                    <p class="insight__description">
                                        <?php echo trim(substr(strip_tags($item->content), 0, 250)); ?>...
                                    </p>
                                    <div class="insight__link-wrapper">
                                        <a href="<?php echo $item->link; ?>" target="_blank" class="insight__link">Read more on Medium</a>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>

                    </div>
                </div>

                <div class="insights__link-wrapper">
                    <a href="https://medium.com/<?php echo $medium_username; ?>" target="_blank" class="btn btn-outline-primary insights__link">Read more on Medium</a>
                </div>
            <?php else: ?>
                <div class="text-center">No articles found.</div>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>
