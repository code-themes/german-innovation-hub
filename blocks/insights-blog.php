<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'insights';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$wp_query = new WP_Query(array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 4
));

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="insights__container">

        <?php if ($wp_query->have_posts()): ?>
            <div class="row justify-content-center">
                <div class="col-md-8 insights__items">

                    <?php $i = 0; while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                    <?php /* if ($i >= 5) { break; } */ ?>
                        <?php get_template_part('templates/insight'); ?>
                    <?php $i++; endwhile; ?>

                </div>
            </div>

            <?php if ($wp_query->max_num_pages > 1): ?>
                <div class="insights__link-wrapper">
                    <a href="" class="btn btn-outline-primary insights__link js-insights-blog-loading-button" data-ajaxurl="<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>" data-posts="<?php echo htmlspecialchars(json_encode($wp_query->query_vars), ENT_QUOTES, 'UTF-8'); ?>" data-current-page="<?php echo get_query_var( 'paged' ) ? get_query_var('paged') : 1; ?>" data-max-page="<?php echo $wp_query->max_num_pages; ?>">Load more insights</a>
                </div>
                <div class="insights__spinner-wrapper d-none">
                    <div class="spinner-grow text-primary insights__spinner" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            <?php endif; ?>

            <?php wp_reset_postdata(); ?>
        <?php else: ?>
            <div class="text-center">No articles found.</div>
        <?php endif; ?>

    </div>
</div>
