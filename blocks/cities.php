<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cities';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$text_left = get_field('text_left') ?: '';
$text_right = get_field('text_right') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="cities__container">

        <?php if (have_rows('items')): ?>
            <div class="row">
                <?php $i = 0; while (have_rows('items')): the_row(); ?>
                            
                    <?php if ($i != 0 && $i % 2 == 0): ?>
                        </div>
                        <div class="row">
                    <?php endif; ?>

                    <div class="col-md-6">

                        <div class="city">

                            <?php $image = get_sub_field('photo'); ?>
                            <?php if (!empty($image)): ?>
                                <div class="city__photo-wrapper">
                                    <?php echo wp_get_attachment_image($image['id'], 'plate-image-540-auto', false, array('class' => 'city__photo')); ?>
                                </div>
                            <?php endif; ?>

                            <div class="city__card">

                                <?php $name = get_sub_field('name'); ?>
                                <?php if (!empty($name)): ?>
                                    <h3><?php echo $name; ?></h3>
                                <?php endif; ?>

                                <?php $address = get_sub_field('address'); ?>
                                <?php if (!empty($address)): ?>
                                    <?php echo $address; ?>
                                <?php endif; ?>

                                <?php $link = get_sub_field('link'); ?>
                                <?php if (!empty($link['title'])): ?>
                                    <div class="city__link-wrapper">
                                        <a href="<?php echo esc_url($link['url']); ?>" class="city__link" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                                    </div>
                                <?php endif; ?>

                            </div>

                        </div>

                    </div>

                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
    <div class="cities__container-text">

        <div class="row">
            <div class="col-md-5 offset-2 offset-md-1">

                <?php echo $text_left; ?>

            </div>
            <div class="col-md-5 offset-2 offset-md-1">

                <?php echo $text_right; ?>

            </div>
        </div>

    </div>
</div>
