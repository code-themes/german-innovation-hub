<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'services';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$columns = get_field('columns');
if (empty($columns)) {
    $columns = 2;
}

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="services__container">

        <?php if (have_rows('items')): ?>
            <div class="row justify-content-center">
                <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        
                    <?php if ($i != 0 && $i % $columns == 0): ?>
                        </div>
                        <div class="row justify-content-center">
                    <?php endif; ?>

                    <div class="col-md-4">

                        <div class="service">
                            <div class="service__content">

                                <?php $image = get_sub_field('icon'); ?>
                                <?php $image_retina = get_sub_field('icon_retina'); ?>
                                <?php $image_width = get_sub_field('icon_width'); ?>
                                <?php $image_height = get_sub_field('icon_height'); ?>
                                <?php if (!empty($image)): ?>
                                    <div class="service__icon-wrapper">
                                        <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="service__icon" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
                                    </div>
                                <?php endif; ?>

                                <?php $heading = get_sub_field('heading'); ?>
                                <?php if (!empty($heading)): ?>
                                    <h3 class="service__heading"><?php echo $heading; ?></h3>
                                <?php endif; ?>

                                <?php $text = get_sub_field('text'); ?>
                                <?php if (!empty($text)): ?>
                                    <p class="service__text"><?php echo $text; ?></p>
                                <?php endif; ?>

                            </div>
                            <div class="service__content-expanded">
                                <?php $text = get_sub_field('text_expanded'); ?>
                                <?php if (!empty($text)): ?>
                                    <?php echo $text; ?>
                                <?php endif; ?>
                            </div>
                            <div class="service__link-wrapper">
                                <a href="" class="service__link js-service-link">
                                    <?php echo get_sub_field('link_text') ?: 'Find out more'; ?>
                                    <span class="service__link-arrow"></span>
                                </a>
                            </div>
                        </div>

                    </div>

                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
