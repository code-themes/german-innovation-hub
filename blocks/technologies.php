<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'technologies';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading') ?: '';
$subheading = get_field('subheading') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="technologies__container">

        <div class="technologies__header">

            <?php if (!empty($heading)): ?>
                <h2 class="technologies__heading"><?php echo $heading; ?></h2>
            <?php endif; ?>

            <?php if (!empty($subheading)): ?>
                <p class="technologies__subheading"><?php echo $subheading; ?></p>
            <?php endif; ?>

        </div>

        <?php if (have_rows('items')): ?>
            <div class="technologies__content">
                <div class="row justify-content-center">
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        <div class="col-6 col-md-4">

                            <?php $image = get_sub_field('icon'); ?>
                            <?php $image_retina = get_sub_field('icon_retina'); ?>
                            <?php $image_width = get_sub_field('icon_width'); ?>
                            <?php $image_height = get_sub_field('icon_height'); ?>
                            <?php if (!empty($image)): ?>
                                <div class="technologies__icon-wrapper">
                                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="technologies__icon" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
                                </div>
                            <?php endif; ?>

                            <?php $title = get_sub_field('title'); ?>
                            <?php if (!empty($title)): ?>
                                <h4 class="technologies__title"><?php echo $title; ?></h4>
                            <?php endif; ?>

                        </div>
                    <?php $i++; endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
