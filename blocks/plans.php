<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'plans';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$note = get_field('note');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="plans__container">

        <?php if (have_rows('plans')): ?>
            <div class="row justify-content-center">
                <?php $i = 0; while (have_rows('plans')): the_row(); ?>
                            
                    <?php if ($i != 0 && $i % 3 == 0): ?>
                        </div>
                        <div class="row justify-content-center">
                    <?php endif; ?>

                    <div class="col-md-4">

                        <div class="plan <?php echo get_sub_field('emphasize') == 1 ? 'plan--emphasize' : ''; ?>">
                            <?php $label = get_sub_field('label'); ?>
                            <?php if (!empty($label)): ?>
                                <div class="plan__label-wrapper">
                                    <span class="plan__label"><?php echo $label; ?></span>
                                </div>
                            <?php endif; ?>
                            <div class="plan__content">

                                <?php $heading = get_sub_field('heading'); ?>
                                <?php if (!empty($heading)): ?>
                                    <h3 class="plan__heading"><?php echo $heading; ?></h3>
                                <?php endif; ?>

                                <?php $text = get_sub_field('text'); ?>
                                <?php if (!empty($text)): ?>
                                    <p class="plan__text"><?php echo $text; ?></p>
                                <?php endif; ?>

                                <?php $link = get_sub_field('button'); ?>
                                <?php if (!empty($link['title'])): ?>
                                    <div class="plan__button-wrapper">
                                        <a href="<?php echo esc_url($link['url']); ?>" class="btn btn-outline-primary plan__button" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <?php $link = get_sub_field('link'); ?>
                            <?php if (!empty($link['title'])): ?>
                                <div class="plan__link-wrapper">
                                    <a href="<?php echo esc_url($link['url']); ?>" class="plan__link <?php echo get_sub_field('emphasize') == 1 ? 'plan__link--red' : ''; ?>" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>">
                                        <?php echo esc_html($link['title']); ?>
                                        <span class="plan__link-arrow"></span>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>

                    </div>

                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($note)): ?>
            <div class="plans__note">
                <?php echo $note; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
