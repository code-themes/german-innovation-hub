<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'partners';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
// ...

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="partners__container">

        <div data-blocks data-blocks-cols="4" data-blocks-item-height="120" data-blocks-breakpoint="768px" data-blocks-breakpoint-cols="2" data-blocks-breakpoint-item-height="100">

            <?php if (have_rows('filters')): ?>
                <div class="partners__controls">
                    <?php $i = 0; while (have_rows('filters')): the_row(); ?>
                        <button type="button" class="partners__controls-button <?php echo $i == 0 ? 'active' : ''; ?>" data-blocks-filter="<?php the_sub_field('tag'); ?>"><?php the_sub_field('text'); ?></button>
                    <?php $i++; endwhile; ?>
                </div>
            <?php endif; ?>

            <?php if (have_rows('items')): ?>
                <div class="partners__logos" data-blocks-list>
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        <div class="partners__logo-wrapper show" data-blocks-item="<?php the_sub_field('tag'); ?>">

                            <?php $image = get_sub_field('logo'); ?>
                            <?php $image_retina = get_sub_field('logo_retina'); ?>
                            <?php $image_width = get_sub_field('logo_width'); ?>
                            <?php $image_height = get_sub_field('logo_height'); ?>
                            <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="partners__logo" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>

                        </div>
                    <?php $i++; endwhile; ?>
                </div>
            <?php endif; ?>

        </div>

    </div>
</div>
