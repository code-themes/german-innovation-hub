<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'goal';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$image = get_field('image') ?: '';
$heading = get_field('heading') ?: '';
$text = get_field('text') ?: '';
$image_right = get_field('image_right') ?: '';
$box_alt = get_field('box_alt') ?: '';

if ($image_right == 1) {
    $className .= ' goal--right';
}

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="goal__container">
        <div class="goal__inner">
            <div class="goal__box <?php echo $box_alt == 1 ? 'goal__box--alt' : ''; ?>" data-paroller-factor="0.1" data-paroller-type="foreground" data-paroller-direction="vertical"></div>

            <?php if (!empty($image)): ?>
                <div class="goal__image-wrapper">
                    <?php echo wp_get_attachment_image($image['id'], 'plate-image-706-auto', false, array('class' => 'goal__image')); ?>
                </div>
            <?php endif; ?>

            <div class="goal__card" data-paroller-factor="-0.1" data-paroller-type="foreground" data-paroller-direction="vertical">

                <?php if (!empty($heading)): ?>
                    <h3 class="goal__heading"><?php echo $heading; ?></h3>
                <?php endif; ?>

                <?php if (!empty($text)): ?>
                    <p class="goal__text"><?php echo $text; ?></p>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
