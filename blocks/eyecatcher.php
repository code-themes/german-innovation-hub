<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'eyecatcher';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
/*$images_landscape = get_field('slides');
$images_portrait = get_field('slides_portrait');

if (empty($images_portrait)) {
    $images_portrait = $images_landscape;
}*/

$slides_with_text = get_field('slides_with_text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php /*<div id="<?php echo esc_attr($id); ?>_landscape_carousel" class="carousel slide eyecatcher__landscape-carousel" data-ride="carousel">

        <?php if ($images_landscape): ?>
            <div class="carousel-inner">
                <?php $i = 0; foreach ($images_landscape as $image): ?>
                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                        <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                    </div>
                <?php $i++; endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($images_landscape) && count($images_landscape) > 1): ?>
            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_landscape_carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_landscape_carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php endif; ?>

    </div>

    <div id="<?php echo esc_attr($id); ?>_portrait_carousel" class="carousel slide eyecatcher__portrait-carousel" data-ride="carousel">

        <?php if ($images_portrait): ?>
            <div class="carousel-inner">
                <?php $i = 0; foreach ($images_portrait as $image): ?>
                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                        <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                    </div>
                <?php $i++; endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($images_portrait) && count($images_portrait) > 1): ?>
            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_portrait_carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_portrait_carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php endif; ?>

    </div>*/?>

    <div id="<?php echo esc_attr($id); ?>_text_carousel" class="carousel slide eyecatcher__text-carousel" data-ride="carousel">

        <?php if ($slides_with_text): ?>
            <div class="carousel-inner">
                <?php $i = 0; foreach ($slides_with_text as $slide): ?>
                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                        <?php echo wp_get_attachment_image($slide['image']['id'], 'full'); ?>
                        <div class="carousel-caption">
                            <div class="eyecatcher__text-carousel-lines">
                                <?php if (!empty($slide['line_1'])): ?>
                                    <h5 class="eyecatcher__text-carousel-line-1"><?php echo $slide['line_1']; ?></h5>
                                <?php endif; ?>
                                <?php if (!empty($slide['line_2'])): ?>
                                    <p class="eyecatcher__text-carousel-line-2"><?php echo $slide['line_2']; ?></p>
                                <?php endif; ?>
                                <?php if (!empty($slide['line_3'])): ?>
                                    <p class="eyecatcher__text-carousel-line-3"><?php echo $slide['line_3']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php $i++; endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($slides_with_text) && count($slides_with_text) > 1): ?>
            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_portrait_carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_portrait_carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php endif; ?>

    </div>

</div>
