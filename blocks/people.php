<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'people';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
// ...

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="people__container">

        <?php if (have_rows('items')): ?>
            <div class="row">
                <?php $i = 0; while (have_rows('items')): the_row(); ?>
                            
                    <?php if ($i != 0 && $i % 3 == 0): ?>
                        </div>
                        <div class="row">
                    <?php endif; ?>

                    <div class="col-md-4">

                        <div class="person people__person">

                            <?php $image = get_sub_field('photo'); ?>
                            <?php if (!empty($image)): ?>
                                <div class="person__photo-wrapper">
                                    <?php echo wp_get_attachment_image($image['id'], 'plate-image-160-160', false, array('class' => 'person__photo')); ?>
                                </div>
                            <?php endif; ?>

                            <div class="person__content">

                                <?php $name = get_sub_field('name'); ?>
                                <?php if (!empty($name)): ?>
                                    <h3 class="person__name"><?php echo $name; ?></h3>
                                <?php endif; ?>

                                <?php $position = get_sub_field('position'); ?>
                                <?php if (!empty($position)): ?>
                                    <h4 class="person__position"><?php echo $position; ?></h4>
                                <?php endif; ?>

                                <?php $contact = get_sub_field('contact'); ?>
                                <?php if (!empty($contact)): ?>
                                    <p class="person__contact"><?php echo $contact; ?></p>
                                <?php endif; ?>

                            </div>

                        </div>

                    </div>

                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
