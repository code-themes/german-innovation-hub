// @ codekit-prepend "../../../bower_components/jquery/dist/jquery.min.js"
// @codekit-prepend "../../../bower_components/popper.js/dist/umd/popper.min.js"
// @codekit-prepend "../../../bower_components/bootstrap/dist/js/bootstrap.min.js"
// @codekit-prepend "../../../bower_components/js-cookie/src/js.cookie.js"
// @codekit-prepend "../../../bower_components/paroller.js/dist/jquery.paroller.js"
// @codekit-prepend "../../../bower_components/autosize/dist/autosize.min.js"

/* global autosize, Cookies */

jQuery(document).ready(function($) {
    // Autosize
    autosize($('textarea'));

    // Parallax
    $('[data-paroller-factor]').paroller();
    $(window).trigger('scroll');

    // Cookie consent
    if (typeof Cookies.get('cookie-consent') === 'undefined') {
        $('.cookie-notice').removeClass('cookie-notice--hidden');
    }

    $('.js-cookie-notice-accept-button').on('click', function (event) {
        event.preventDefault();

        Cookies.set('cookie-consent', 'true', {
            expires: 365*5
        });

        $('.cookie-notice').addClass('cookie-notice--hidden');
    });

    $('.js-cookie-notice-decline-button').on('click', function (event) {
        event.preventDefault();

        Cookies.remove('cookie-consent');

        $('.cookie-notice').addClass('cookie-notice--hidden');
    });

    // Mobile menu
    $('.js-mobile-menu-open-button').on('click', function (event) {
        event.preventDefault();

        $('.mobile-menu-wrapper')
            .removeClass('mobile-menu-wrapper--hidden')
            .find('.mobile-menu-wrapper__left')
            .addClass('animated slideInLeft')
            .siblings('.mobile-menu-wrapper__right')
            .addClass('animated slideInRight')
            .on('animationend', function () {
                $('.mobile-menu-wrapper')
                    .find('.mobile-menu-wrapper__left')
                    .removeClass('animated slideInLeft')
                    .siblings('.mobile-menu-wrapper__right')
                    .removeClass('animated slideInRight')
                    .off('animationend');
            });

        $('body')
            .addClass('mobile-menu-open')
            .append('<div class="mobile-menu-backdrop"></div>');

        $('.mobile-menu-backdrop').on('click', function () {
            $('.js-mobile-menu-close-button').trigger('click');
        });
    });

    $('.js-mobile-menu-close-button').on('click', function (event) {
        event.preventDefault();

        $('.mobile-menu-wrapper')
            .addClass('mobile-menu-wrapper--animating')
            .find('.mobile-menu-wrapper__left')
            .addClass('animated slideOutLeft')
            .siblings('.mobile-menu-wrapper__right')
            .addClass('animated slideOutRight')
            .on('animationend', function () {
                $('.mobile-menu-wrapper')
                    .removeClass('mobile-menu-wrapper--animating')
                    .addClass('mobile-menu-wrapper--hidden')
                    .find('.mobile-menu-wrapper__left')
                    .removeClass('animated slideOutLeft')
                    .siblings('.mobile-menu-wrapper__right')
                    .removeClass('animated slideOutRight')
                    .off('animationend');
            });

        $('body').removeClass('mobile-menu-open');

        $('.mobile-menu-backdrop').remove();
    });
    
    // Header
    $(window).scroll(function () {
        var header = $('.header');
        var headerOffset = header.offset().top + header.innerHeight();

        // WP admin bar
        headerOffset = headerOffset - parseInt($('html').css('margin-top'));

        if ($(window).scrollTop() >= headerOffset) {
            $('.mobile-header')
                .removeClass('mobile-header--hidden')
                .addClass('animated slideInDown');
        } else {
            $('.mobile-header')
                .addClass('mobile-header--hidden')
                .removeClass('animated slideInDown');
        }
    });

    // Service
    $('.service').each(function () {
        var contentHeight = $(this).children('.service__content').innerHeight();
        var linkWrapperHeight = $(this).children('.service__link-wrapper').innerHeight();

        $(this).innerHeight(contentHeight + linkWrapperHeight);
    });

    $('.js-service-link').on('click', function (event) {
        event.preventDefault();

        var service = $(this).closest('.service');
        var contentHeight = service.children('.service__content').innerHeight();
        var contentExpandedHeight = service.children('.service__content-expanded').innerHeight();
        var linkWrapperHeight = service.children('.service__link-wrapper').innerHeight();

        if (!service.hasClass('open')) {
            service
                .addClass('open')
                .innerHeight(contentHeight + contentExpandedHeight + linkWrapperHeight);
        } else {
            service
                .removeClass('open')
                .innerHeight(contentHeight + linkWrapperHeight);
        }
    });

    // Accordion
    $('.accordion__item').each(function () {
        var linkWrapperHeight = $(this).children('.accordion__link-wrapper').innerHeight();

        $(this).innerHeight(linkWrapperHeight);
    });

    $('.js-accordion-link').on('click', function (event) {
        event.preventDefault();

        var accordionItem = $(this).closest('.accordion__item');
        var linkWrapperHeight = accordionItem.children('.accordion__link-wrapper').innerHeight();
        var contentHeight = accordionItem.children('.accordion__content').innerHeight();

        if (!accordionItem.hasClass('open')) {
            accordionItem
                .addClass('open')
                .innerHeight(linkWrapperHeight + contentHeight);
        } else {
            accordionItem
                .removeClass('open')
                .innerHeight(linkWrapperHeight);
        }
    });

    // Window resize
    $(window).resize(function () {
        // Service
        $('.service').each(function () {
            var contentHeight = $(this).children('.service__content').innerHeight();
            var contentExpandedHeight = $(this).children('.service__content-expanded').innerHeight();
            var linkWrapperHeight = $(this).children('.service__link-wrapper').innerHeight();

            if (!$(this).hasClass('open')) {
                $(this).innerHeight(contentHeight + linkWrapperHeight);
            } else {
                $(this).innerHeight(contentHeight + contentExpandedHeight + linkWrapperHeight);
            }
        });

        // Accordion
        $('.accordion__item').each(function () {
            var linkWrapperHeight = $(this).children('.accordion__link-wrapper').innerHeight();
            var contentHeight = $(this).children('.accordion__content').innerHeight();

            if (!$(this).hasClass('open')) {
                $(this).innerHeight(linkWrapperHeight);
            } else {
                $(this).innerHeight(linkWrapperHeight + contentHeight);
            }
        });
    });

    // Scroll to anchors
    $('a[href^=\\#]').on('click', function (event) {
        var anchor = $(this).attr('href');

        if (anchor === '#' ||
            $(this).hasClass('nav-link') ||
            $(this).hasClass('carousel-control-prev') ||
            $(this).hasClass('carousel-control-next')) {
            return;
        }

        event.preventDefault();

        if (anchor === '#newsletter-modal' || anchor === '#newsletter') {
            $('#newsletter-modal').modal('show');
            return;
        }

        var scrollHeader = $('.mobile-header');
        var scrollHeaderHeight = scrollHeader.css('position') === 'fixed' ? scrollHeader.innerHeight() : 0;

        // WP admin bar
        var wpAdminBar = parseInt($('html').css('margin-top'));

        $('html, body').animate({
            scrollTop: $(anchor).offset().top - scrollHeaderHeight - 15 - wpAdminBar
        }, 'slow');
    });

    // Blog loading button
    $('.js-insights-blog-loading-button').on('click', function (event) {
        event.preventDefault();

        var linkWrapper = $(this).closest('.insights__link-wrapper');
        var linkWrapperHeight = linkWrapper.innerHeight();
        var spinnerWrapper = linkWrapper.next('.insights__spinner-wrapper');

        var insightsWrapper = $(this).closest('.insights');
        var items = insightsWrapper.find('.insights__items');

        var ajaxurl = $(this).data('ajaxurl');
        var posts = $(this).data('posts');
        var current_page = $(this).data('current-page');
        var max_page = $(this).data('max-page');

        var data = {
            'action': 'loadmore',
            'query': posts,
            'page' : current_page
        };
 
        $.ajax({ // you can also use $.post here
            url: ajaxurl, // AJAX handler
            data: data,
            type: 'POST',
            context: $(this),
            beforeSend: function (xhr) {
                linkWrapper.addClass('d-none');
                spinnerWrapper.innerHeight(linkWrapperHeight);
                spinnerWrapper.removeClass('d-none');
            },
            success: function(data) {
                if (data) { 
                    spinnerWrapper.addClass('d-none');
                    linkWrapper.removeClass('d-none');

                    items.append(data); // insert new posts

                    current_page = current_page + 1;
                    $(this).data('current-page', current_page);
 
                    if (current_page == max_page) { 
                        // if last page, remove the button
                        linkWrapper.addClass('d-none');
                    }

                    // you can also fire the "post-load" event here if you use a plugin that requires it
                    // $(document.body).trigger('post-load');
                } else {
                    // if no data, remove the button as well
                    linkWrapper.addClass('d-none');
                }
            }
        });
    });
    
    // Blocks
    function updateBlocks(wrapper) {
        wrapper = $(wrapper);
        var breakpoint = wrapper.data('blocks-breakpoint');
        var list = wrapper.find('[data-blocks-list]');
        var listWidth = list.width();
        var items = list.find('[data-blocks-item]').filter('.show');

        // Items per row
        var cols = wrapper.data('blocks-cols') * 1;
        if (window.matchMedia('(max-width: '+breakpoint+')').matches) {
            cols = wrapper.data('blocks-breakpoint-cols') * 1;
        }

        var rows = Math.round(items.length/cols);
        var itemWidth = listWidth / cols;
        var itemHeight = null;
        var listHeight = null;
        var rowIndex = null;
        var columnIndex = null;
        var currentRowHeight = null;
        var currentItemHeight = null;
        var nextRowHeight = null;

        // Height of the list
        var itemHeightInput = wrapper.data('blocks-item-height');
        if (window.matchMedia('(max-width: '+breakpoint+')').matches) {
            itemHeightInput = wrapper.data('blocks-breakpoint-item-height');
        }

        if (itemHeightInput === 'auto') {
            rowIndex = 0;
            columnIndex = 0;

            currentRowHeight = 0;
            currentItemHeight = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;

                    listHeight = listHeight + currentRowHeight;
                    currentRowHeight = 0;
                    currentItemHeight = 0;
                }

                currentItemHeight = $(this).innerHeight();

                if (currentItemHeight > currentRowHeight) {
                    currentRowHeight = currentItemHeight;
                }

                columnIndex = columnIndex + 1;
            });

            listHeight = listHeight + currentRowHeight;
        } else {
            itemHeight = itemHeightInput * 1;
            listHeight = rows*itemHeight;
        }

        list.css('height', listHeight + 'px');

        // Position the items
        if (itemHeightInput === 'auto') {
            rowIndex = 0;
            columnIndex = 0;

            currentRowHeight = 0;
            currentItemHeight = 0;
            nextRowHeight = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;

                    nextRowHeight = currentRowHeight;
                    currentRowHeight = 0;
                    currentItemHeight = 0;
                }

                currentItemHeight = $(this).innerHeight();

                if (currentItemHeight > currentRowHeight) {
                    currentRowHeight = currentItemHeight;
                }

                $(this).css('top', nextRowHeight + 'px');
                $(this).css('left', (columnIndex*itemWidth) + 'px');

                columnIndex = columnIndex + 1;
            });
        } else {
            rowIndex = 0;
            columnIndex = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;
                }

                $(this).css('top', (rowIndex*itemHeight) + 'px');
                $(this).css('left', (columnIndex*itemWidth) + 'px');

                columnIndex = columnIndex + 1;
            });
        }
    }
    
    $('[data-blocks]').each(function () {
        updateBlocks(this);
    });

    $('[data-blocks-filter]').on('click', function () {
        var link = $(this);
        var linkTarget = link.attr('data-blocks-filter');
        var linkSiblings = link.siblings();
        var blocksSelector = '[data-blocks-item="'+linkTarget+'"]';

        if (linkTarget === '') {
            blocksSelector = '[data-blocks-item]';
        }

        var wrapper = link.closest('[data-blocks]');
        var allBlocks = wrapper.find('[data-blocks-item]');
        var showBlocks = wrapper.find(blocksSelector);
        var showingBlocks = showBlocks.filter('.hidden');
        var hiddenBlocks = allBlocks.not(showBlocks);
        var hidingBlocks = hiddenBlocks.filter('.show');

        linkSiblings.removeClass('active');
        link.addClass('active');

        hidingBlocks.removeClass('hiding hidden showing show').addClass('hidden');
        showingBlocks.removeClass('hiding hidden showing show').addClass('show');

        updateBlocks(wrapper);
    });

    $(window).resize(function () {
        $('[data-blocks]').each(function () {
            updateBlocks(this);
        });
    });
});
