<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = '';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$images = get_field('images') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="row justify-content-center mb-5">
        <div class="col-12">

            <div class="custom-carousel">
                <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel">
                    <?php if ($images): ?>
                        <div class="carousel-inner">
                            <?php $i = 0; foreach ($images as $image): ?>
                                <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                                    <?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => 'd-block w-100 h-auto')); ?>
                                </div>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?php if (count($images) > 1): ?>
                        <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>
