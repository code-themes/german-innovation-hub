<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = '';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$indented = get_field('indented') ?: '';
$items = get_field('items') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="row justify-content-center mb-5">
        <div class="<?php echo !empty($indented) ? 'col-md-10' : 'col-12'; ?>">

            <?php if ($items): ?>
                <div class="accordion">
                    <div class="accordion__col">
                        <?php $i = 1; foreach ($items as $item): $i++; ?>
                            <?php if ($i % 2 !== 0) { continue; } ?>
                            <div class="accordion__item">
                                <div class="accordion__link-wrapper">
                                    <a href="" class="accordion__link js-accordion-link">
                                        <?php echo $item['heading']; ?>
                                        <span class="accordion__link-icon"></span>
                                    </a>
                                </div>
                                <div class="accordion__content">
                                    <?php echo $item['text']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="accordion__col">
                        <?php $i = 1; foreach ($items as $item): $i++; ?>
                            <?php if ($i % 2 === 0) { continue; } ?>
                            <div class="accordion__item">
                                <div class="accordion__link-wrapper">
                                    <a href="" class="accordion__link js-accordion-link">
                                        <?php echo $item['heading']; ?>
                                        <span class="accordion__link-icon"></span>
                                    </a>
                                </div>
                                <div class="accordion__content">
                                    <?php echo $item['text']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
