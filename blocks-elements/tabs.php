<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = '';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$items = get_field('items') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="row justify-content-center mb-5">
        <div class="<?php echo !empty($indented) ? 'col-md-10' : 'col-12'; ?>">

            <?php if ($items): ?>
                <ul class="nav nav-tabs" id="<?php echo esc_attr($id); ?>_myTab" role="tablist">
                    <?php $i = 1; foreach ($items as $item): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $i == 1 ? 'active' : ''; ?>" id="<?php echo esc_attr($id); ?>_tab-<?php echo $i; ?>-link" data-toggle="tab" href="#<?php echo esc_attr($id); ?>_tab-<?php echo $i; ?>" role="tab" aria-controls="<?php echo esc_attr($id); ?>_tab-<?php echo $i; ?>" aria-selected="true"><?php echo $item['name']; ?></a>
                        </li>
                    <?php $i++; endforeach; ?>
                </ul>
                <div class="tab-content" id="<?php echo esc_attr($id); ?>_myTabContent">
                    <?php $i = 1; foreach ($items as $item): ?>
                        <div class="tab-pane fade <?php echo $i == 1 ? 'show active' : ''; ?>" id="<?php echo esc_attr($id); ?>_tab-<?php echo $i; ?>" role="tabpanel" aria-labelledby="<?php echo esc_attr($id); ?>_tab-<?php echo $i; ?>-link">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $item['text_left']; ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo $item['text_right']; ?>
                                </div>
                            </div>
                        </div>
                    <?php $i++; endforeach; ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
