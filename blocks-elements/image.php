<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = '';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$image = get_field('image') ?: '';
$caption = get_field('caption') ?: '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="row justify-content-center mb-5">
        <div class="col-12">

            <?php if (!empty($image)): ?>
                <figure class="figure">
                    <?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => 'figure-img img-fluid')); ?>

                    <?php if (!empty($caption)): ?>
                        <figcaption class="figure-caption"><?php echo $caption; ?></figcaption>
                    <?php endif; ?>
                </figure>
            <?php endif; ?>

        </div>
    </div>
</div>
